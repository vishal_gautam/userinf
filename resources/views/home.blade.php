<!DOCTYPE html>
<html lang="en">
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
</head>
<body>
<div class="container">
    <H1>User Information</H1>
    <table class="table table-dark">
    <thead>
        <tr>
        <th scope="col">First</th>
        <th scope="col">Last</th>
        <th scope="col">Email Address</th>
        </tr>
    </thead>
    <tbody>
    @foreach($data as $key)
        <tr>
        <th scope="row">{{$key[0]}}</th>
        <td>{{$key[1]}}</td>
        <td>{{$key[2]}}</td>
        </tr>
    @endforeach    
    </tbody>
    </table>

</div> 
    <!-- <ul>
        @foreach($data as $key)
            <li>
                @foreach($key as $k)
                <li>{{$k}}</li>
                @endforeach
            </li>

        @endforeach
    </ul> -->
    
</body>
</html>

 <!-- <?php
        // if(empty($data)){

        //     print "No data found";
        //     }
        // else{
        //     $mask = "%10s %-10s %s\n";
        //     foreach ($data as $row){
        //     echo sprintf($mask, $row[0], $row[1], $row[2]);
        //     }
        // }
    
    
    ?>  -->