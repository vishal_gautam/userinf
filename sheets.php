<?php
function fun(){
require __DIR__.'/vendor/autoload.php';

$client = new \Google_client();
$client->setApplicationName('Google Sheets and PHP');
$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
$client->setAccessType('offline');
$client->setAuthConfig(__DIR__.'/credentials.json');
$service = new Google_Service_Sheets($client);
$spreadsheetId= "1PBmEvGCLVBOPHOVoSfJUVhE5pLqSV2Y5gat_3KviPvk";

$range = "RD!A2:Z1000";
$response = $service->spreadsheets_values->get($spreadsheetId, $range);
$values = $response->getValues();

return $values;
}
// function fun()
// {
//     $data= $values;
//     return 'data';
// }
// if(empty($values)){

// print "No data found";
// }
// else{
//     $mask = "%10s %-10s %s\n";
//     foreach ($values as $row){
//         echo sprintf($mask, $row[0], $row[1], $row[2]);
//     }
// }
?>