<?php

require __DIR__.'/../vendor/autoload.php';

$client = new \Google_client();
$client->setApplicationName('Google Sheets and PHP');
$client->setScopes([\Google_Services_Sheets::SPREADSHEETS]);
$client->setAccessType('offline');
$client->setAuthConfig(__DIR__ . '/credentials.json');
$service = new Google_Services_Sheets($client);
$spreadsheetId= "1PBmEvGCLVBOPHOVoSfJUVhE5pLqSV2Y5gat_3KviPvk";

$range = "RD!A1:Z1000";
$response = $service->spreadsheets_values->get($spreadsheetId, $range);
$values = $response->getValues();
if(empty($values)){

print "No data found";
}
else{
    $mask = "%10s %-10s %S\n";
    foreach ($values as $row){
        echo sprintf($mask, $row[2], $row[1], $row[0]);
    }
}
?>